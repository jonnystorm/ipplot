//go:build profiling

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package main

import (
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"runtime/pprof"
)

func init() {
	profilePath := "heap.profile"

	f, err := os.Create(profilePath)
	if err != nil {
		panic(fmt.Sprintf("failed to create profile path %s: %v", profilePath, err))
	}
	pprof.WriteHeapProfile(f)

	go func() {
		defer f.Close()
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()
}
