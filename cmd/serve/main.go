package main

import (
	"fmt"
	"net/http"
)

func main() {
	port := 8080
	portStr := fmt.Sprintf("%d", port)
	fmt.Println("Serving wasm directory on http://localhost:" + portStr + "...")

	http.ListenAndServe(":"+portStr, http.FileServer(http.Dir("./wasm")))

	return
}
