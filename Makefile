ipplot:
	go build -o ../../bin -C cmd/ipplot #-tags profiling

wasm:
	cd cmd/ipplot && gogio -target js -o ../../wasm .

serve:
	go build -o ../../bin -C cmd/serve

clean:
	rm bin/ipplot || true
	rm -r wasm || true
	rm bin/serve || true

