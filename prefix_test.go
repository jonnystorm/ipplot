package ipplot

import (
	"testing"
)

func TestLessThanOrEqFailure1(t *testing.T) {
	pfx0, _ := NewIPv4([]byte{198, 18, 0, 0}, 15)
	ipb0 := NewIPBlock(pfx0, IANAReserved)

	pfx1, _ := NewIPv4([]byte{198, 175, 251, 92}, 32)
	ipb1 := NewIPBlock(pfx1, Allocation)

	pfx2, _ := NewIPv4([]byte{203, 0, 113, 0}, 24)
	ipb2 := NewIPBlock(pfx2, IANAReserved)

	// this was failing; caused by bug in lessThan logic
	if ipb1.LessThanOrEq(ipb0) {
		t.Errorf("should not be left of %s", pfx0)
	}
	if ipb2.LessThanOrEq(ipb1) {
		t.Errorf("should not be right of %s", pfx2)
	}
	if !ipb1.LessThanOrEq(ipb2) {
		t.Errorf("should be left of %s", pfx2)
	}
}
