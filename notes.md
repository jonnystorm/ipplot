## 2023-10-06

Rather than having IPv4- and IPv6-specific block structures,
I'm leaning towards a single block structure from which to
infer various domain-specific properties.

* How would this affect our ability to classify blocks by
  protocol?
* How to ensure correct behavior without type-based bit
  limits? Might we accidentally turn v4 into v6, etc?
* How do we cleanly separate "block" behavior from "IP"
  behavior?
* What kind of block is a block? We Morton encode $2^{\*w}$
  and a block is a kind of finite denizen of the new space.

> Thought: another way to map a network graph onto an
> address space is with a kind of quadtree compression, and
> the resulting "pixels" are the largest which contain a
> single point, resulting in a kind of least upper bound.
>
> As it stands, this notion requires a god's-eye view of the
> whole graph, but perhaps this can be readily distributed.

```go
func prefixLessThan(o1, m1, o2, m2 byte) bool {
	f1 := int(o1&m1) - int((o1^byte(255))&m1)
	f2 := int(o2&m2) - int((o2^byte(255))&m2)
	return f1 < f2 && o1 != o2&m1 || m1 > m2
}
```
