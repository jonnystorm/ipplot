/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package main

import (
	"log"
	"os"

	"gioui.org/app"
	"gioui.org/io/system"
	"gioui.org/layout"
	"gioui.org/op"
	"gioui.org/widget/material"

	"idio.link/go/ipplot"
)

func main() {
	go func() {
		w := app.NewWindow()
		th := material.NewTheme()
		a := App{window: w, theme: th}
		if err := a.run(); err != nil {
			log.Fatal(err)
		}
		os.Exit(0)
	}()
	app.Main()
}

type App struct {
	window *app.Window
	theme  *material.Theme
}

func (a *App) run() error {
	ipp := ipplot.NewIPPlot(a.theme)
	var ops op.Ops
	for {
		e := <-a.window.Events()
		switch e := e.(type) {
		case system.DestroyEvent:
			return e.Err
		case system.FrameEvent:
			gtx := layout.NewContext(&ops, e)
			ipp.Layout(gtx)
			e.Frame(gtx.Ops)
		}
	}
}
