module idio.link/go/ipplot

go 1.15

require (
	gioui.org v0.3.1
	github.com/go-text/typesetting v0.0.0-20230905121921-abdbcca6e0eb // indirect
	golang.org/x/exp v0.0.0-20231006140011-7918f672742d // indirect
	golang.org/x/exp/shiny v0.0.0-20231006140011-7918f672742d // indirect
	golang.org/x/image v0.13.0 // indirect
)
